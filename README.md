# Apprentissage automatique

## Description du projet 
Le projet a été réalisé dans le cadre du cours d'Apprentisage automatique
à l'INALCO (TAL)

## Contenu du projet 
- rapport du projet (fichier doc/projet_yliya.pdf)
- notebooks contenant le code python : 
1) lstm_bidir_emb_chars_12-auteurs_courte-version (programme principal en version courte (corpus partiel) de reponctuation de textes littéraires et de stylométrie)
2) corpus (sorties contenant l'information statistique sur le corpus)
3) lstm_bidir_emb_chars_12-auteurs_tuned (programme principal optimisé à l'aide de fine-tuning) 
